import random
import matplotlib.pyplot as plt
import plotly.graph_objects as go

# ideal data to ds conversion
# using prior code
dic_companies = {
    "AACC": ["AACCFF"],
    "ANPAC": ["ANPACFF"],
    "ASPEN": ["ASPFF"],
    "CENT": ["CENT_FF", "CENT_"],
    "FALCONRET": ["FALC"],
    "REPWEST": ["REPWESTFF"],
    "UNIQ": ["UNIQFF"],
    "NEWBIE": ["Unassigned Client"],
    "HUNT": ["Hunt, John"],
}
dic_company_data = {}
for company in dic_companies.keys():
    # assuming index is company name
    # assuming row is the billed fee
    for index, row in df.iterrows():
        if index in dic_companies[company]:
            dic_company_data[company] = row
"""
dic_company_data would have as an example
dic_company_data
{"AACC": 19, "ANPAC": 20}
"""
# company fees
fees = []
# company names
companies = []
# iterate and add company name, fee to lists
for company, fee in dic_company_data.values():
    fees.append(fee)
    companies.append(company)

colors = []
for i in range(len(companies)):
    color = random.randrange(0, 2**24)
    hex_color = hex(color)
    colors.append(hex_color)


# 3d pie chart in matplot
# chart for data i.e billed fees
explode = ()
for i in range(len(companies)):
    explode.append(0.05)

fig = plt.figure(figsize=(10, 7))
ax = fig.add_subplot(111, projection="3d")
ax.pie(
    fees,
    colors=colors,
    companies=companies,
    autopct="%1.1f%%",
    startangle=90,
    pctdistance=0.85,
    explode=explode,
)

ax.set_title("Billed Fees")
plt.legend(labels, loc="best")
plt.show()

# 3d pie chart with plotly

fig = go.Figure(
    data=[
        go.Pie(
            companies=companies,
            fees=fees,
            hole=0.3,
            marker=dict(colors=colors, line=dict(color="#000000", width=2)),
        )
    ]
)

# add title and legend
ax.set_title("Billed Fees")
plt.legend(companies, loc="best")

# show chart
plt.show()
