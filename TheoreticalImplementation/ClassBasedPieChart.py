from decimal import *


class PieSlice:
    def __init__(self, company_name, value, percentage):
        self.company_name = company_name
        self.value = value
        self.percentage = percentage

    def __str__(self):
        return f"{self.company_name} with a bill value of {self.value} has a percentage of {self.percentage}"


class PieChart:
    def __init__(self, slices=None, sum_of_slices_values=0, pie_percentage=100):
        # most basic implementation
        # rest of implementation is for []
        if slices is None:
            slices = []
        """
		!it could also be

		if slices is None:
			slices={}

		if required to perform frequent lookups or searches on slices, use dict or set
		dict conisting of key value pairs, unordered, no duplicates (would have to handle collisions)
		set being unordered, mutable, iterable, no duplicates, not key value pairs

		dict={
				"company x":19
				"company y":20
		}
		will have to have a different implemntatin of methods
		"""
        self._slices = slices
        self.amount_of_slices = len(self._slices)
        self.sum_of_slices_values = sum_of_slices_values
        self._pie_percentage = pie_percentage

    def __str__(self):
        return f"There are {self.amount_of_slices} slices"

    def add_slice(self, name, value):
        new_slice = PieSlice(name, value, 0)
        self._slices.append(new_slice)
        self.sum_of_slices_values += value
        for slice in self._slices:
            slice.percentage = Decimal((slice.value / self.sum_of_slices_values) * 100)

    def remove_slice(self, name):
        for slice in self._slices:
            if slice.company_name == name:
                self.sum_of_slices_values -= slice.value
                self._slices.remove(slice)
                for slice in self._slices:
                    slice.percentage = Decimal(
                        (slice.value / self.sum_of_slices_values) * 100
                    )
                break

    def iterate_over_slices(self):
        for slice in self._slices:
            print(
                f"Company {slice} has a value of {self.value} with a percentage of {self.percentage}"
            )

    def display_pie_chart(self):
        pass
