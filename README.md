# Billed Fee Updates

**April 24 2023**

(Initial Meeting Notes)

Goal of Billed Fee Project - Automate process to generate a financial report using script that manipulates excel data → write logic and then use Power Bi

- [Automation Notes](Automation/Notes)
- [Automation File](Automation/BilledFeeReport.py)

## __Automation Notes__
- [April 28th](Automation/Notes/April28th.md)
- [May 6th](Automation/Notes/May6th.md)
- [May 12th](Automation/Notes/May12th.md)
- [May 19th](Automation/Notes/May19th.md)

Desired outcome of Billed Fee Project (Bain and Barkley)

- Refactor code → Component Based (Modularize → Convert into Objects)
    - Automated process that generates a Financial Report
    - Use Python to manipulate Excel Data (read excel data → iterate/sift through data → use information in array format to render into pie chart with legend and diagram)
    - Additionally have a OOP implementation (???)

        ```python
        class PieSlice():
        	def __init__(self, company_name, value, percentage):
        		self.company_name=company_name
        		self.value=value
        		self.percentage=percentage
        	def __str__(self):
        		return f'{self.company_name} with a bill value of {self.value} has a percentage of {self.percentage}'

        class PieChart():
        	def __init__(self, slices=None, sum_of_slices_values=0, pie_percentage=100):
        		#most basic implementation
        		#rest of implementation is for []
        		if slices is None:
        			slices=[]
        		"""
        		!it could also be

        		if slices is None:
        			slices={}

        		if required to perform frequent lookups or searches on slices, use dict or set
        		dict conisting of key value pairs, unordered, no duplicates (would have to handle collisions)
        		set being unordered, mutable, iterable, no duplicates, not key value pairs

        		dict={
        				"company x":19
        				"company y":20
        		}
        		"""
        		self._slices=slices
        		self.amount_of_slices=len(self._slices)
        		self.sum_of_slices_values=sum_of_slices_values
        		self._pie_percentage=pie_percentage
        	def __str__(self):
        		return f'There are {self.amount_of_slices} slices'

        	def add_slice(self,name,value):
        		new_slice=PieSlice(name, value, 0)
        		self._slices.append(new_slice)
        		self.sum_of_slices_values+=value
        		for slice in self._slices:
        			slice.percentage=Decimal((slice.value/self.sum_of_slices_values)*100)

        	def remove_slice(self,name):
        		for slice in self._slices:
        			if slice.company_name==name:
        				self.sum_of_slices_values-=slice.value
        				self._slices.remove(slice)
        				for slice in self._slices:
        					slice.percentage=Decimal((slice.value/self.sum_of_slices_values)*100)
        				break

        	def iterate_over_slices(self):
        		for slice in self._slices:
        			print(f'Company {slice} has a value of {self.value} with a percentage of {self.percentage}')

        	def display_pie_chart(self, self._slices):
        		#theoretical below
        ```

        ```python
        import random
        import matplotlib.pyplot as plt
        import plotly.graph_objects as go

        # ideal data to ds conversion
        # using prior code
        dic_companies = {
            "AACC": ["AACCFF"],
            "ANPAC": ["ANPACFF"],
            "ASPEN": ["ASPFF"],
            "CENT": ["CENT_FF", "CENT_"],
            "FALCONRET": ["FALC"],
            "REPWEST": ["REPWESTFF"],
            "UNIQ": ["UNIQFF"],
            "NEWBIE": ["Unassigned Client"],
            "HUNT": ["Hunt, John"],
        }
        dic_company_data = {}
        for company in dic_companies.keys():
            # assuimg index is company name
            # assuming row is the billed fee
            for index, row in df.iterrows():
                if index in dic_companies[company]:
                    dic_company_data[company] = row
        """
        dic_company_data would have as an example
        dic_company_data
        {"AACC": 19, "ANPAC": 20}
        """
        # company fees
        fees = []
        # company names
        companies = []
        # iterate and add company name, fee to lists
        for company, fee in dic_company_data.values():
            fees.append(fee)
            companies.append(company)

        colors = []
        for i in range(len(companies)):
            color = random.randrange(0, 2**24)
            hex_color = hex(color)
            colors.append(hex_color)
        ```

        ```python
        #chart for data i.e billed fees
        explode=()
        for i in range(len(companies)):
        	explode.append(.05)

        fig = plt.figure(figsize=(10,7))
        ax = fig.add_subplot(111, projection='3d')
        ax.pie(fees, colors = colors, companies=companies, autopct='%1.1f%%', startangle=90, pctdistance=0.85, explode = explode)

        ax.set_title("Billed Fees")
        plt.legend(labels, loc="best")
        plt.show()
        ```

        ```python
        #3d pie chart with plotly
        fig = go.Figure(data=[go.Pie(companies=companies, fees=fees, hole=.3,
                                     marker=dict(colors=colors, line=dict(color='#000000', width=2)))])

        # add title and legend
        ax.set_title("Billed Fees")
        plt.legend(companies, loc="best")

        # show chart
        plt.show()
        ```

- Question: Is there an emphasis on interactivity as well as it being a 3d pie chart?
    - Does interactivity have a higher priority/emphasis over it being a 3d chart?
    - What type of interactivity is desired?
- Regarding libraries for implementation of 3D Pie Chart
    - Matplotlib:
        - Pros
            - Widely used plotting library that provides a wide range of customization options for high quality plots
        - Cons
            - Steeper learning curve as compared to other libraries due to complex API
    - Plotly
        - Pros
            - Interactive plots with hover-over tools, zoom capabilities that allows for easier use for interactive visualizations vs Matplotlib
        - Cons
            - More limited range of customization options vs Matplotlib
- What type of Pie Chart is Ideal? (3d pie chart aesthetics)

![One2DChart](Images/SeventhChart.png)

![Untitled](Images/FourthChart.png)

(1st attempt, 04/27)

- Performed an initial investigation regarding the construction of 3D pie charts in python, the most immediate result was ChartDirector
- The problem with ChartDirector is that one would have to pay for a licensed version in order to remove a yellow label that is included as a watermark, despite attempts to remove in imported files it remained (requires a license for removal), would still allow me for importing excel file data as a pd and iterating over rows - con is price, use of 3rd party software (it did create some nice charts)

![FirstPieChart](Images/FirstPieChart.png)

![IconPie](Images/iconpie.png)

![IconDonut](Images/icondonut.png)

- [Chartit Django](https://readthedocs.org/projects/django-chartit/)
    - ![Untitled](Images/FifthChart.png)

- [Using matplotlib](https://www.geeksforgeeks.org/plot-a-pie-chart-in-python-using-matplotlib/)
    - ![Sixth Chart](Images/SixthChart.png)
        - Customized Pie Chart

- [Using SjVisualizer, Canvas](https://www.youtube.com/watch?v=zzfZ0BnKZaQ)
    - ![Seventh Chart](Images/ThirdChart.png)
        - Interactive, Animated Chart
