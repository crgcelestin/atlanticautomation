# Billed Fees Documentation
## General Purpose of Billed Fee Report.py
- Python script that reads data from Excel file and processes it to generate report of billed fees for various clients
- Script creates Excel File with report -> format report -> save the formatted report
- Create pie  chart (plt.pie) of data utilizing the matplotlib library and display chart

## Reading and Writing Data
- Panda library is used to read data from excel file using path, the we store what's read from excel using read_excel from the pandas library -> store info in the df variable
- ### Data Processing
    -
