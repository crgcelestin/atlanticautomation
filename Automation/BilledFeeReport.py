import pandas as pd
import math, datetime
import matplotlib.pyplot as plt
import openpyxl
from openpyxl.styles import Alignment
from openpyxl.styles import Font
from openpyxl.cell import Cell


df = pd.read_excel(
    r"P:/Users/Craig/Desktop/CS/AtlanticAutomation/Automation/AtlanticAuto/Billing.xlsx"
)

company_names = set()
dic_companies = {
    "AACC": ["AACCFF"],
    "ANPAC": ["ANPACFF"],
    "ASPEN": ["ASPFF"],
    "CENT": ["CENT_FF", "CENT_"],
    "FALCONRET": ["FALC"],
    "REPWEST": ["REPWESTFF"],
    "UNIQ": ["UNIQFF"],
    "NEWBIE": ["Unassigned Client"],
    "HUNT": ["Hunt, John"],
}
for index, row in df.iterrows():
    if str(row["Client"]) != "nan":
        company_names.add(str(row["Client"]).split(".")[0])

final_df = pd.DataFrame([], [], columns=["Client", "Billed Fees"])
for company in company_names:
    fees = 0.0
    for index, row in df.iterrows():
        if company == str(row["Client"]).split(".")[0]:
            fees += row["Billable Fees"]
    for key in dic_companies:
        if company in dic_companies[key]:
            company = key
            print(company)
            break
    frac, whole = math.modf(fees)
    if frac < 0.5:
        fees = math.floor(fees)
    else:
        fees = math.ceil(fees)

    final_df = final_df.append(
        {"Client": company, "Billed Fees": fees}, ignore_index=True
    )
# print(final_df['Billed Fees'].sum())
un_names = set(final_df["Client"])
unique_df = pd.DataFrame([], [], columns=["Client", "Billed Fees"])
for name in un_names:
    fees = 0
    for index, row in final_df.iterrows():
        if name == str(row["Client"]):
            fees += row["Billed Fees"]
    unique_df = unique_df.append(
        {"Client": name, "Billed Fees": fees}, ignore_index=True
    )
total_fee = unique_df["Billed Fees"].sum()
res_df = pd.DataFrame([], [], columns=["Client", "Billed Fees", "Percentage"])
for index, row in unique_df.iterrows():
    # print(type(row['Billed Fees']))
    if row["Billed Fees"] > 0:
        per = (row["Billed Fees"] / total_fee) * 100
        per = round(per, 2)
        res_df = res_df.append(
            {
                "Client": row["Client"],
                "Billed Fees": row["Billed Fees"],
                "Percentage": per,
            },
            ignore_index=True,
        )
res_df = res_df.sort_values(by=["Billed Fees"], ascending=False)
res_df.insert(0, "Total", "Total")
res_df.at[0, "Billed Fees"] = total_fee

writer = pd.ExcelWriter(
    r"P:/Users/Craig/Desktop/CS/AtlanticAutomation/Automation/AtlanticAuto/Billed_Fees_Report.xlsx",
    engine="openpyxl",
)
res_df.to_excel(writer, index=False)

ws = writer.sheets["Sheet1"]
ws["C2"].number_format = "0.00%"
ws["B2"].number_format = '_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)'

colors = []
for index, row in res_df.iterrows():
    if row["Percentage"] < 2:
        colors.append("gray")
    else:
        colors.append("")

data = res_df["Percentage"]
mylabels = res_df["Client"]
plt.pie(data, labels=mylabels, colors=colors, autopct="%1.1f%%", startangle=90)
plt.title(f'{datetime.datetime.now().strftime("%B %Y")} Billed Fees')
plt.legend(title="Clients", loc="center left", bbox_to_anchor=(1, 0, 0.5, 1))
plt.axis("equal")
plt.show()

for index, row in res_df.iterrows():
    if row["Percentage"] < 2:
        ws.cell(row=index + 2, column=3).font = Font(color="gray")
    else:
        color = colors[index]
        ws.cell(row=index + 2, column=3).fill = openpyxl.styles.PatternFill(
            start_color=color, end_color=color, fill_type="solid"
        )

writer.save()
