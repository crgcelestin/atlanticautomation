## MAY 6TH
### Refining Data Visualization
- #### Provided Files
    -   Billing.xlsx
        - [Billing File](Automation/AtlanticAuto/Billing.xlsx)
        - Input File containing raw tabulated data of Billed Fees


    - Billed_Fees_by_Company_2023.xslx
        - [Billed Fees by Company 2023](Automation/AtlanticAuto/Billed_Fees_by_Company_2023.xlsx)
        - Close to expected output of business i.e. customer specifications
            -  __Expected Chart__
            ![Expected Chart](Automation/Images/ExpectedChart.png)


    - Billed_Fees_Report_April
      - [April Billed Fees](Automation/AtlanticAuto/Billed_Fees_Report_April.xlsx)
      - Current bad output produced by python code, severely illegible pie chart
        - __Bad Chart Output__
        ![Bad Chart](Automation/Images/BadOutput.png)
