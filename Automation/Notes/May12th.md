# __May 12th__
- Meeting Notes
    - Financial Report Impacting Revenue - Owner of Law Firm is the End User: Steve Bain (Bain and Barkley)

[ Power Bi ]

- Create data driven culture with business intelligence

    - Enable all at every org level to make confident decisions with up to date analytics
        - Self service analytics @ enterprise scale - mitigate cost, complexity, security risks of several solutions w/ platform that scales from indviduals to orgs as whole
        - Smart tools for strong results - find, share insights with data visualizations, built in Ai capability, tight excel integration, pre built + custom data connectors
        - Protect analytical data - keep data secure w/ industry leading data security capabilities w/ sensitivity labeling, end to end encryption + real time access monitoring
