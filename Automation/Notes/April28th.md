## APRIL 28TH
### General Purpose of Billed Fee Report.py
- Python script that reads data from Excel file and processes it to generate report of billed fees for various clients
- Script creates Excel File with report -> format report -> save the formatted report
- Create pie  chart (plt.pie) of data utilizing the matplotlib library and display chart

### Reading and Writing Data
- Panda library is used to read data from excel file using path, then we store what's read from excel using read_excel from the pandas library -> store info in the df variable

    ```py3
    import pandas as pd
    df = pd.read_excel(
        r"P:\Users\Dulce\Steve's Reports\Monthly Reports\Billed Fees Reports\Input\Billing.xlsx"
    )
     ```
- #### Data Processing
    1. initialize company dictionary store and provide companies with their aliases as keys, potential equivalent names as values

        ```py3
            company_names = set()
            dic_companies = {
                "AACC": ["AACCFF"],
                "ANPAC": ["ANPACFF"],
                "ASPEN": ["ASPFF"],
                "CENT": ["CENT_FF", "CENT_"],
                "FALCONRET": ["FALC"],
                "REPWEST": ["REPWESTFF"],
                "UNIQ": ["UNIQFF"],
                "NEWBIE": ["Unassigned Client"],
                "HUNT": ["Hunt, John"],
            }
        ```
    2. Iterate over rows of input Excel file using .iterrows()
        ```py3
            for index, row in df.iterrows():
                if str(row["Client"]) != "nan":
                    company_names.add(str(row["Client"]).split(".")[0])
        ```
    3. Extract client names and then calculate total billed fees per client
        - There are several fees per client so the eventual final_df would be a dict of each company name and the sum of fees in aggregate for each company

        ```py3
        final_df = pd.DataFrame([], [], columns=["Client", "Billed Fees"])
        for company in company_names:
            fees = 0.0
            for index, row in df.iterrows():
                if company == str(row["Client"]).split(".")[0]:
                    fees += row["Billable Fees"]
            for key in dic_companies:
                if company in dic_companies[key]:
                    company = key
                    print(company)
                    break
            frac, whole = math.modf(fees)
            if frac < 0.5:
                fees = math.floor(fees)
            else:
                fees = math.ceil(fees)

            final_df = final_df.append(
                {"Client": company, "Billed Fees": fees}, ignore_index=True
            )
        # print(final_df['Billed Fees'].sum())
        un_names = set(final_df["Client"])
        unique_df = pd.DataFrame([], [], columns=["Client", "Billed Fees"])
        for name in un_names:
            fees = 0
            for index, row in final_df.iterrows():
                if name == str(row["Client"]):
                    fees += row["Billed Fees"]
            unique_df = unique_df.append(
                {"Client": name, "Billed Fees": fees}, ignore_index=True
            )
        ```
    3. Calculate total billed fees and percentages for each client and create final report w/ data in descending order

        ```py3
            res_df = pd.DataFrame([], [], columns=["Client", "Billed Fees", "Percentage"])
            for index, row in unique_df.iterrows():
                # print(type(row['Billed Fees']))
                if row["Billed Fees"] > 0:
                    per = (row["Billed Fees"] / total_fee) * 100
                    per = round(per, 2)
                    res_df = res_df.append(
                        {
                            "Client": row["Client"],
                            "Billed Fees": row["Billed Fees"],
                            "Percentage": per,
                        },
                        ignore_index=True,
                    )
            res_df = res_df.sort_values(by=["Billed Fees"], ascending=False)
            res_df.insert(0, "Total", "Total")
            res_df.at[0, "Billed Fees"] = total_fee
        ```

    4. Resulting report is written to an Excel file and with formatting applied to data and headings

        ```py3
            writer = pd.ExcelWriter(
            r"P:\Users\Dulce\Steve's Reports\Monthly Reports\Billed Fees Reports\Billed_Fees_Report.xlsx",
            engine="openpyxl",
            )
            res_df.to_excel(writer, index=False)

            ws = writer.sheets["Sheet1"]
            ws["C2"].number_format = "0.00%"
            ws["B2"].number_format = '_("$"* #,##0.00_);_("$"* \(#,##0.00\);_("$"* "-"??_);_(@_)'
        ```

    5. Script applies conditional formatting to percentage column, with entires highlighted in grey that have percentages less than 2%

        ```py3
            colors = []
            for index, row in res_df.iterrows():
                if row["Percentage"] < 2:
                    colors.append("gray")
                else:
                    colors.append("")
        ```

    6. Then utilizes matplotlib for the creation of a pie chart of data and then displays a visualization
        -   The chart has percentages displayed for each client and color of slice and a legend that shows each name of client
        - below is formatting for the chart

        ```py3
            data = res_df["Percentage"]
            mylabels = res_df["Client"]
            plt.pie(data, labels=mylabels, colors=colors, autopct="%1.1f%%", startangle=90)
            plt.title(f'{datetime.datetime.now().strftime("%B %Y")} Billed Fees')
            plt.legend(title="Clients", loc="center left", bbox_to_anchor=(1, 0, 0.5, 1))
            plt.axis("equal")
            plt.show()
        ```

    - I'm assuming this is for the legend but it could also be for excel chart formatting then save it to the desired excel file

        ```py3
        for index, row in res_df.iterrows():
            if row["Percentage"] < 2:
                ws.cell(row=index + 2, column=3).font = Font(color="gray")
            else:
                color = colors[index]
                ws.cell(row=index + 2, column=3).fill = openpyxl.styles.PatternFill(
                    start_color=color, end_color=color, fill_type="solid"
                )
        writer.save()
        ```
