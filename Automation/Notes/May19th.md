# __MAY 19TH__
## Meeting Notes
### Python v Javascript as a Developer

- Gathering data and enabling analytical decisions to decipher a yes vs no is critical

- Focus ony Python: Django, Automation, Cybersecurity, Scripting
    - __Python Route__
        - Resume (sharing experiences)
            * Talk about Power Bi in interview, solving real world problems
        - Interviewers/Hiring Managers desire to see real world experience especially that a candidate has deployed to production
            - Leveraged (Support + Maintain) PowerBi -> Production for a Law Firm (Steve Bain of Bain and Barkley Law Firm)
            - Production: Deployed a Dashboard that supported the monitoring of targeted items
                - Example: Craig came into finish product for LF involving said details
                - Challenge Faced (Speak about Pain Points)
                    - Unable to log into local machine -> Had to figure out a work around to run code on PC
                    - Access Issues: As a secure enterprise system, code is running from a remote machine

    - AA (Atlantic Automation) is a consulting company
        - Consults law firms on Process Automation
            - Automate work done by paralegals
        - Has worked on a number of difference products
    - __Project Scope__
        - With B&B specifically, had to inherit code with the current project being a Financial Report that puts business results i.e clients fees aggregated in a Power Bi Dashboard framework
            - 2 week sprint involving VMware and Jira
            - Billed Fees (Communicate with Business Stakeholder)
                - Insurance Defense Law Firm that bills Insurance Companies as clients
                - LF needs to be aware of number of clients, revenue, number of clients required for sufficient profit
                - KPI Dashboard
                    - Shows amount of money that each client is being billed for

- __Action Items__
    - Resume Finalization
    - Code in VM
        - Utilize New Login
        - Access and Run Code -> Understand Output
        - Look at live updates
            - Want Dashboard to output when running report req that the DB update w/ Power Automate
                - Python Code -> Dashboard
                - make.powerautomate.com
    - Process Automation
        - Test -> Output Works -> Validation
        - Power Automate Flow Monthly
            - Input (Updated Monthly Data) -> Py Script -> Refresh Data Set (Excel File) -> Current Dashboard
